
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  FlatList,
  ActivityIndicator,
  Modal,
  ScrollView
} from 'react-native';
import {Button, Tile, List, ListItem, Overlay, SearchBar} from 'react-native-elements';
import {API} from './constants/';
import {UserModal} from './UserModal';

export class Users extends Component<Props> {

  constructor(props) {
      super(props);
      this.state = {
          users: [],
          page: 1,
          isOverlayVisible: false,
          userDetails: {},
          searchParamerter: 'abc',
          loading: false,
          refreshing:false,
          error: null,
          modalVisible: false
      }
  }

  componentWillMount() {
    this.getUserList()
  }
  
  getUserList = () => {
        const {page, searchParamerter} = this.state;
        //console.log(`PAGE: ${page} :: searchparameter: ${searchParamerter}`);
        
        const url = `${API.getUsers}?q=${searchParamerter}&page=${page}&per_page=20`;
        console.log(`URL: ${url} :: PAGE: ${page} :: SEARCH: ${searchParamerter}`); 
        this.setState({ loading: true });
        fetch(url)
            .then((users) => users.json())
            .then((userList) => {
                //console.log(`users: ${JSON.stringify(users)}`)
                this.setState({
                    users: page === 1 ?  userList.items : [...this.state.users, ...userList.items],
                    error: userList.error || null,
                    loading: false,
                    refreshing: false
                    
                });
                //console.log(`ITEMS: ${JSON.stringify(userList.items[0])}`);  
            })
            .catch(error => {
                this.setState({error, loading: false})
            })
  }

  fetchUserNames = () => {
    const url = `${API.user}`;
    fetch(url)
          .then((user) => user.json())
          .then((user) => {
              //console.log(`users: ${JSON.stringify(users)}`)
              this.setState({userDetails: user});
              console.log(`ITEMS: ${JSON.stringify(users.items[0])}`);
              
          })
  }

  getUserDetails = (item) => {
      const url = `${item.url}`;
      fetch(url)
            .then((user) => user.json())
            .then((user) => {
                this.setState({userDetails: user, modalVisible: true});
            })
    };
  

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "86%",
          backgroundColor: "#CED0CE",
          marginLeft: "14%"
        }}
      />
    );
  };

  renderFooter = () => {
    if (!this.state.loading) return null;

    return (
        <View
        style={{
            paddingVertical: 20,
            borderTopWidth: 1,
            borderColor: "#CED0CE"
        }}
        >
        <ActivityIndicator animating size="large" />
        </View>
    );
    };

    clearText = () => {
        this.search.clearText();
    }

    searchUsers = (text) => {
        this.setState({
            page: 1,
            searchParamerter: text
        }, () => {
            this.getUserList();
        })
    }

  renderHeader = () => {
    return (
        <SearchBar
            ref={search => this.search = search}
            placeholder="Type Here..." 
            lightTheme 
            round
            onChangeText={(text) => this.searchUsers(text)}
            onCancel={this.clearText}
            onClearText={(text) => this.searchUsers(text)}
            //platform="ios"
            cancelButtonTitle="Cancel"
        />
    );
   };

   handleLoadMore = () => {
        // console.log(`handleloadMore called`);
         
        this.setState({
            page: this.state.page + 1
        },
        () => {
        this.getUserList();
        }
        );
    };

    _keyExtractor = (item, index) => item.id

    overlay = () => {
        return (
            <Overlay isVisible={this.state.isOverlayVisible}>
                <Text>{this.state.userDetails}</Text>
            </Overlay>
        );
    }

    closeModal = () => {
        this.setState({
            modalVisible: false
        })
    }
  
  render() {
    //console.log(`USERS item count: ${JSON.stringify(this.state.users)}`);
    return (
        <View style={{flex:1}}>
            <UserModal 
                isVisible={this.state.modalVisible} 
                closeModal={() => this.closeModal()}
                userDetails={this.state.userDetails}
            />
            <List containerStyle={{flex:1, borderTopWidth: 0, borderBottomWidth: 0 }}>
                {this.renderHeader()}
                <FlatList
                    data={this.state.users}
                    keyExtractor={item => item.id}
                    ItemSeparatorComponent={this.renderSeparator}
                    onEndReached={this.handleLoadMore}
                    onEndReachedThreshold={0.5}
                    ListFooterComponent={this.renderFooter}
                    renderItem={({item}) => {
                        //console.log(`user: ${JSON.stringify(item)}`)
                        return (
                            <ListItem
                                key={item.id}
                                roundAvatar
                                avatar={{uri: item.avatar_url}}
                                containerStyle={{ borderBottomWidth: 0 }}
                                //title={`${item.name.first.toUpperCase()} ${item.name.last.toUpperCase()}`}
                                subtitle={item.url}
                                onPress={() => this.getUserDetails(item)}
                            />
                        );
                    }}
                />
            </List>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    //backgroundColor: 'grey',
  },
  innerContainer: {
    alignItems: 'center',
  }
});
