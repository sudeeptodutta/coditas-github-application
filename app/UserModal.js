import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  FlatList,
  ActivityIndicator,
  Modal,
  ScrollView
} from 'react-native';
import {Button, Tile, List, ListItem, Overlay, SearchBar} from 'react-native-elements';
import {API} from './constants/';

export const UserModal = (props) =>{
    if (props.isVisible) {
        console.log(`USER DETAILS: ${JSON.stringify(props.userDetails)}`);
        const {avatar_url: picture, name, bio, login, location, email, blog, followers} = props.userDetails;
        return (
                <View style={styles.container}>
                <Modal
                    visible={props.isVisible}
                    animationType={'slide'}
                    onRequestClose={() => props.closeModal()}
                >
                    <View style={styles.modalContainer}>
                        <View style={styles.innerContainer}>
                            <ScrollView>
                                <Tile
                                imageSrc={{ uri: picture}}
                                featured
                                title={name ? `${name.toUpperCase()}` : 'No Name'}
                                caption={bio || 'No Caption'}
                                />
                                <List>
                                    <ListItem
                                        title="Username"
                                        rightTitle={login}
                                        hideChevron
                                    />
                                    <ListItem
                                        title="Email"
                                        rightTitle={email || 'No Email'}
                                        hideChevron
                                    />
                                    <ListItem
                                        title="Blog"
                                        rightTitle={blog || 'No Blogs'}
                                        hideChevron
                                    />
                                    <ListItem
                                        title="Followers"
                                        rightTitle={followers.toString() || 'No followers'}
                                        hideChevron
                                    />
                                    <ListItem
                                        title="City"
                                        rightTitle={location || 'Unknown'}
                                        hideChevron
                                    />
                                </List>
                            </ScrollView>
                            <Button
                                containerViewStyle={{marginTop: 10}}
                                buttonStyle={{backgroundColor: 'skyblue'}}
                                onPress={() => props.closeModal()}
                                iconRight={{name: 'close'}}
                                title='CLOSE'
                                borderRadius={5}
                            >
                            </Button>
                        </View>
                    </View>
                </Modal>
                </View>
        );
    } else return null;
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
    modalContainer: {
      flex: 1,
      justifyContent: 'center',
      //backgroundColor: 'grey',
    },
    innerContainer: {
      alignItems: 'center',
    }
});