# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Coditas Github Application.
* Version : 0.0.1

### How do I get set up? ###

* `$ git clone https://sudeeptodutta@bitbucket.org/sudeeptodutta/coditas-github-application.git`
* `$ cd coditas-github-application`
* `$ yarn` (or npm install)
* `$ react-native run-ios` (for running inside ios Simulator)
* `$ react-native run-android` (for running inside android Simulator)`




### Possible Issues ###

1. **Unrecognized font family '(Icon pack name)'**
    * Close running packager
    * Run `react-native link react-native-vector-icons`
    * After that run `react-native start --reset-cache`
    * Finally use `react-native run-ios`

